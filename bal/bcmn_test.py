#!/bin/env python
import sys
import time
import bcnode

from mininet.topo import MinimalTopo
from mininet.net import Mininet
from mininet.cli import CLI
from mininet.node import Host, CPULimitedHost, RemoteController
from mininet.util import specialClass
from bal.QTopo import QTopo


net = Mininet(  topo=QTopo(),
                controller=lambda name: RemoteController(name, defaultIP='127.0.0.1'),
                listenPort=6634)

net.start()

for node in net.hosts:
    node.start()

time.sleep(2)

for node in net.hosts:
    print(node.call("chain"))

result=CLI(net)

for node in net.hosts:
    node.stop()
net.stop()
